usb-serial-for-uniapp

#### 介绍
uniApp USB 串口通信插件，基于usb-serial-for-android 

#### 软件架构
基于Android的原生uniapp插件。

项目构成：

app模块：vue 页面

usbSerialForAndroid模块：基于usb-serial-for-android 的USB驱动

usbSerialForUniAPP模块：集成uniapp与usb-serial-for-android 的API接口


#### 安装教程

1.  uniapp插件市场：https://ext.dcloud.net.cn/
2.  搜索usb-serial-for-uniapp
3.  点击右侧按钮“下载插件并导入HBuilderX”
4.  打开HBuilderX 并选择需要引入插件的项目
5.  uniapp代码地址：https://gitee.com/jason_hua/usb-serial-for-uniapp-demo

#### 接口说明

```java
	/**
     * 获取可用USB端口
     * @return 所有USB驱动的列表
     */
    @UniJSMethod (uiThread = false)
    public JSONObject getUsbDevices()

    /**
     * 获取某个USB端口
     * @param usbDevice 要打开的Usb Device name
     * @return 成功或失败的提示，以JSON格式返回
     */
    @UniJSMethod (uiThread = false)
    public JSONObject connect(String usbDevice)

    /**
     * 获取某个USB端口
     * @return 成功或失败的提示，以JSON格式返回
     */
    @UniJSMethod (uiThread = false)
    public JSONObject disconnect()

    /**
     * 获取某个USB端口
     * @return 关闭串口，成功或失败的提示，以JSON格式返回
     * baudRate 波特率
     * dataBits 数据位
     * stopBits 停止位
     * parity 校验位
     */
    @UniJSMethod (uiThread = false)
    public JSONObject getUsbConf()

    /**
     * 配置某个USB端口
     * @param jsonStr JsonString 格式的参数
     *      baudRate 波特率
     *      dataBits 数据位
     *      stopBits 停止位
     *      parity 校验位
     * @return 成功或失败的提示，以JSON格式返回
     */
    @UniJSMethod (uiThread = false)
    public JSONObject setUsbConf(String jsonStr)

    /**
     * 发送数据到USB端口
     * @param data 要发送到端口的数据
     * @param format 数据的格式 String or HxeString
     * @return 成功或失败的提示，以JSON格式返回
     */
    @UniJSMethod (uiThread = false)
    public JSONObject snedData (String data , String format)
    /**
     * 发送数据到USB端口
     * @param data 要发送到端口的数据
     * @param format 数据的格式 String or HxeString
     * @param intervalTime 轮询时间间隔 ms
     * @return 成功或失败的提示，以JSON格式返回
     */
    @UniJSMethod (uiThread = false)
    public JSONObject snedData (String data , String format ,int intervalTime)

    /**
     * 添加USB端口监听器
     * @param format 返回值格式 String or HexString
     * @return 成功或失败的提示，以JSON格式返回
     */
    @UniJSMethod(uiThread = false)
    public JSONObject addUsblistener (String format)
        
    /**
     * 设置监听数据的UDP发送
     * @param code    业务数据explain
     * @param ip         服务器IP
     * @param port       UDP端口
     * @param isOpen  打开关闭发送服务
     * @return 成功或失败的提示，以JSON格式返回
     */
    @UniJSMethod(uiThread = false)
    public JSONObject setUDPSend (String code,String ip,int port,boolean isOpen) 
```

#### 特殊说明

- 更新uni-app离线打包支持，HBuilderX（3.8.7.20230703）

- 升级SDK至Android-SDK@3.8.7.81902_20230704.zip

- 升级后debug需要配置apkkey，如遇“未配置appkey或配置错误”，参考[官方说明](https://nativesupport.dcloud.net.cn/AppDocs/usesdk/appkey.html#)

- 证书生成说明

  ```shell
  cd C:\Java\jdk1.8.0_202\bin
  
  .\keytool.exe -genkey -alias key0 -keyalg RSA -keysize 2048 -validity 36500 -keystore test.keystore
  #key0是证书别名，可修改为自己想设置的字符，建议使用英文字母和数字
  #test.keystore是证书文件名称，可修改为自己想设置的文件名称
  
  #可以使用以下命令查看
  .\keytool.exe -list -v -keystore test.keystore
  ```

  

