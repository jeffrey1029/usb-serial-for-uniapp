package com.ghost.uniplugin;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import io.dcloud.feature.uniapp.annotation.UniJSMethod;
import io.dcloud.feature.uniapp.common.UniModule;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;

import com.ghost.android.usbserial.driver.UsbSerialDriver;
import com.ghost.android.usbserial.driver.UsbSerialPort;
import com.ghost.android.usbserial.driver.UsbSerialProber;
import com.ghost.android.usbserial.util.SerialInputOutputManager;

import static io.dcloud.common.util.ReflectUtils.getApplicationContext;


public class UsbSerialAPI extends UniModule implements SerialInputOutputManager.Listener{

    String TAG = "UsbSerialAPI";
    public static int REQUEST_CODE = 1000;
    //缓存USB驱动信息
    private JSONArray temp = new JSONArray();
    private Map<String,Integer> tempPort = new HashMap<String,Integer>();
    //缓存USB驱动
    private Map<String,UsbSerialDriver> driverTemp = new HashMap<String,UsbSerialDriver>();
    //Android USB 管理器
    private UsbManager usbManager;
    //usb 默认探测器
    private UsbSerialProber usbDefaultProber;
    //usb 自定义探测器
    private UsbSerialProber usbCustomProber;
    //usb 串口号
    private UsbSerialPort usbSerialPort;
    //usb 输入输出管理器，用于监听数据
    private SerialInputOutputManager usbIoManager;
    //usb 权限状态
    private enum UsbPermission { Unknown, Requested, Granted, Denied };
    private UsbPermission usbPermission = UsbPermission.Unknown;
    //usb 连接状态
    private boolean connected = false;
    private static final String INTENT_ACTION_GRANT_USB = BuildConfig.LIBRARY_PACKAGE_NAME + ".GRANT_USB";
    //usb 写数据超时时间
    private static final int WRITE_WAIT_MILLIS = 1000;
    //usb 读数据超时时间
    private static final int READ_WAIT_MILLIS = 1000;
    //usb 配置参数
    private int baudRate, dataBits, stopBits, parity;
    //usb 返回数据的格式。String or HexString
    private String resultFormat="String";
    //usb 循环发送线程
    private Thread sendThread=null;

    private boolean explain = false;

    //是否打开UDP发送
    private boolean isOpenUDP = false;
    //UDP发送配置
    private DatagramSocket datagramSocket;
    //UDP发送地址
    private InetAddress address;
    //UDP发送端口
    private int port= 44555;
    //业务数据
    private String code= "carCode";



    public UsbSerialAPI () throws SocketException, UnknownHostException {
        this.usbManager = (UsbManager) getApplicationContext().getSystemService(Context.USB_SERVICE);
        this.usbDefaultProber = UsbSerialProber.getDefaultProber();
        this.usbCustomProber = CustomProber.getCustomProber();
        this.baudRate = 9600;
        this.dataBits = 8;
        this.stopBits = UsbSerialPort.STOPBITS_1;
        this.parity = UsbSerialPort.PARITY_NONE;
    }

    /**
     * 获取可用USB端口
     * @return 所有USB驱动的列表
     */
    @UniJSMethod (uiThread = false)
    public JSONObject getUsbDevices(){
        JSONObject result = new JSONObject();
        for(UsbDevice device : usbManager.getDeviceList().values()) {
            UsbSerialDriver driver = usbDefaultProber.probeDevice(device);
            if(driver != null) {
                for(int port = 0; port < driver.getPorts().size(); port++){
                    JSONObject usb = new JSONObject();
                    usb.put("port", port);
                    usb.put("name", driver.getClass().getSimpleName().replace("SerialDriver",""));
                    usb.put("vendor", device.getVendorId());
                    usb.put("product", device.getProductId());
                    temp.add(usb);
                    tempPort.put(driver.getClass().getSimpleName().replace("SerialDriver",""),port);
                    driverTemp.put(driver.getClass().getSimpleName().replace("SerialDriver",""),driver);
                }

            } else {
                driver = usbCustomProber.probeDevice(device);
                JSONObject usb = new JSONObject();
                usb.put("port", 0);
                usb.put("name", driver.getClass().getSimpleName().replace("SerialDriver",""));
                usb.put("vendor", device.getVendorId());
                usb.put("product", device.getProductId());
                temp.add(usb);
                tempPort.put(driver.getClass().getSimpleName().replace("SerialDriver",""),port);
                driverTemp.put(driver.getClass().getSimpleName().replace("SerialDriver",""),driver);
            }
        }

        if(temp.size()==0){
            result.put("code", 500);
            result.put("data", "usb driver not found");
        }else {
            result.put("code", 200);
            result.put("data", temp);
        }
        return result;
    }

    /**
     * 获取某个USB端口
     * @param usbDevice 要打开的Usb Device name
     * @return 成功或失败的提示，以JSON格式返回
     */
    @UniJSMethod (uiThread = false)
    public JSONObject connect(String usbDevice){
        JSONObject result = new JSONObject();
        UsbSerialDriver driver = driverTemp.get(usbDevice);
        if(driver == null) {
            result.put("code", 500);
            result.put("data", "connection failed: no driver for device");
            return result;
        }
        int portNum = Integer.valueOf(tempPort.get(usbDevice).toString());
        if(driver.getPorts().size() < portNum) {
            result.put("code", 500);
            result.put("data", "connection failed: not enough ports at device");
            return result;
        }
        usbSerialPort = driver.getPorts().get(portNum);
        UsbDeviceConnection usbConnection = usbManager.openDevice(driver.getDevice());
        if(usbConnection == null && usbPermission == UsbPermission.Unknown && !usbManager.hasPermission(driver.getDevice())) {
            usbPermission = UsbPermission.Requested;
            PendingIntent usbPermissionIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent(INTENT_ACTION_GRANT_USB), 0);
            usbManager.requestPermission(driver.getDevice(), usbPermissionIntent);
        }
        if(usbConnection == null) {
            if (!usbManager.hasPermission(driver.getDevice())) {
                result.put("code", 500);
                result.put("data", "connection failed: permission denied");
                return result;
            }else{
                result.put("code", 500);
                result.put("data", "connection failed: open failed");
                return result;
            }
        }
        try {
            usbSerialPort.open(usbConnection);
            usbSerialPort.setParameters(this.baudRate, this.dataBits, this.stopBits, this.parity);
            connected = true;
        } catch (Exception e) {
            disconnect();
            result.put("code", 500);
            result.put("data", "connection failed: " + e.getMessage());
            return result;
        }
        result.put("code", 200);
        result.put("data", usbDevice + " is connected");
        return result;
    }

    /**
     * 获取某个USB端口
     * @return 成功或失败的提示，以JSON格式返回
     */
    @UniJSMethod (uiThread = false)
    public JSONObject disconnect(){
        JSONObject result = new JSONObject();
        connected = false;
        try {
            usbSerialPort.close();
        } catch (IOException e) {
            result.put("code", 500);
            result.put("data", "disconnect failed: " + e.getMessage());
            return result;
        }
        usbSerialPort = null;
        result.put("code", 200);
        result.put("data","usbDevice is disconnected");
        return result;
    }

    /**
     * 获取某个USB端口
     * @return 关闭串口，成功或失败的提示，以JSON格式返回
     * baudRate 波特率
     * dataBits 数据位
     * stopBits 停止位
     * parity 校验位
     */
    @UniJSMethod (uiThread = false)
    public JSONObject getUsbConf(){
        JSONObject result = new JSONObject();
        JSONObject data = new JSONObject();
        data.put("baudRate", this.baudRate);
        data.put("dataBits", this.dataBits);
        data.put("stopBits", this.stopBits);
        data.put("parity", this.parity);
        result.put("code", 200);
        result.put("data", data.toString());
        return result;
    }

    /**
     * 配置某个USB端口
     * @param jsonStr JsonString 格式的参数
     *      baudRate 波特率
     *      dataBits 数据位
     *      stopBits 停止位
     *      parity 校验位
     * @return 成功或失败的提示，以JSON格式返回
     */
    @UniJSMethod (uiThread = false)
    public JSONObject setUsbConf(String jsonStr) {
        JSONObject result = new JSONObject();
        JSONObject options = JSON.parseObject(jsonStr);
        this.baudRate = Integer.valueOf(options.get("baudRate").toString());
        this.dataBits =Integer.valueOf(options.get("dataBits").toString());
        this.stopBits =Integer.valueOf(options.get("stopBits").toString());
        this.parity =Integer.valueOf(options.get("parity").toString());
        try {
            usbSerialPort.setParameters(this.baudRate, this.dataBits, this.stopBits, this.parity);
        } catch (IOException e) {
            result.put("code", 500);
            result.put("data", "connection failed: " + e.getMessage());
            return result;
        }
        result.put("code", 200);
        result.put("data", "OK");
        return result;
    }

    /**
     * 发送数据到USB端口
     * @param data 要发送到端口的数据
     * @param format 数据的格式 String or HxeString
     * @return 成功或失败的提示，以JSON格式返回
     */
    @UniJSMethod (uiThread = false)
    public JSONObject snedData (String data , String format){
        JSONObject result = new JSONObject();
        if(!connected) {
            result.put("code", 500);
            result.put("data", "not connected");
            return result;
        }
        byte[] databyte;
        switch (format){
            case "HexString":
                databyte = HexDump.hexStringToByteArray(data);
                break;
            default:
                databyte = (data).getBytes();
                break;
        }
        try {
            usbSerialPort.write(databyte, WRITE_WAIT_MILLIS);
        } catch (Exception e) {
            result.put("code", 500);
            result.put("data", "SnedData failed: " + e.getMessage());
        }
        result.put("code", 200);
        result.put("data", HexDump.dumpHexString(databyte));
        return result;
    }

    /**
     * 发送数据到USB端口
     * @param data 要发送到端口的数据
     * @param format 数据的格式 String or HxeString
     * @param intervalTime 轮询时间间隔 ms
     * @return 成功或失败的提示，以JSON格式返回
     */
    @UniJSMethod (uiThread = false)
    public JSONObject snedData (String data , String format ,int intervalTime){
        JSONObject result = new JSONObject();
        if(!connected) {
            result.put("code", 500);
            result.put("data", "not connected");
            return result;
        }
        byte[] databyte;
        switch (format){
            case "HexString":
                databyte = HexDump.hexStringToByteArray(data);
                break;
            default:
                databyte = (data).getBytes();
                break;
        }
        try {
            Runnable send = new Sned(usbSerialPort,databyte,intervalTime);
            if (sendThread!=null) {
                sendThread.interrupt();
                sendThread.stop();
            }
            sendThread = new Thread(send);
            sendThread.start();
        } catch (Exception e) {
            result.put("code", 500);
            result.put("data", "SnedData failed: " + e.getMessage());
        }
        result.put("code", 200);
        result.put("data", HexDump.dumpHexString(databyte));
        return result;
    }

    /**
     * 读取USB端口数据
     * @param format 数据的格式 String or HxeString
     * @param readLength 读取数据的长度（16进制位数）
     * @return 读取的数据，以JSON格式返回
     */
    @UniJSMethod (uiThread = false)
    public JSONObject readData (String format,int readLength){
        JSONObject result = new JSONObject();
        if(!connected) {
            result.put("code", 500);
            result.put("data", "not connected");
            return result;
        }
        byte[] buffer = new byte[readLength];
        int len=0;
        try {
            len = usbSerialPort.read(buffer, READ_WAIT_MILLIS);
        } catch (Exception e) {
            result.put("code", 500);
            result.put("data", "SnedData failed: " + e.getMessage());
        }
        String data="";
        switch (format){
            case "HexString":
                data = HexDump.toHexString(buffer,0,len);
                break;
            default:
                data = HexDump.dumpHexString(buffer,0,len);
                break;
        }
        result.put("code", 200);
        result.put("data", data);
        return result;
    }

    /**
     * 添加USB端口监听器
     * @param format 返回值格式 String or HexString
     * @return 成功或失败的提示，以JSON格式返回
     */
    @UniJSMethod(uiThread = false)
    public JSONObject addUsblistener (String format) {
        JSONObject result = new JSONObject();
        resultFormat=format;
        usbIoManager = new SerialInputOutputManager(usbSerialPort, this);
        Executors.newSingleThreadExecutor().submit(usbIoManager);
        result.put("code", 200);
        result.put("data", "Usb listener is Open");
        return result;
    }

    /**
     * 设置监听数据的UDP发送
     * @param code    业务数据explain
     * @param ip         服务器IP
     * @param port       UDP端口
     * @param isOpen  打开关闭发送服务
     * @return 成功或失败的提示，以JSON格式返回
     */
    @UniJSMethod(uiThread = false)
    public JSONObject setUDPSend (String code,String ip,int port,boolean isOpen) {
        JSONObject result = new JSONObject();
        this.port=port;
        try {
            datagramSocket = new DatagramSocket(port);
            address = InetAddress.getByName(ip);
            this.code = code;
            isOpenUDP = isOpen;
        } catch (Exception e) {
            result.put("code", 500);
            result.put("data", "set UDP failed: " + e.getMessage());
        }
        result.put("code", 200);
        result.put("data", "UDP is Open");
        return result;
    }

    /**
     * 设置监听数据解析
     * @param isOpen  打开关闭数据解析
     * @return 成功或失败的提示，以JSON格式返回
     */
    @UniJSMethod(uiThread = false)
    public JSONObject setExplain (boolean isOpen) {
        JSONObject result = new JSONObject();
        try {
            explain = isOpen;
        } catch (Exception e) {
            result.put("code", 500);
            result.put("data", "set explain failed: " + e.getMessage());
        }
        result.put("code", 200);
        result.put("data", "explain is Open");
        return result;
    }

    /**
     * 端口监听方法，调用uniapp 监听事件 mUniSDKInstance
     * @param data 监听到的数据 byte[]
     */
    @Override
    public void onNewData(byte[] data) {
        String result = "";
        if(resultFormat != null) {
            switch (resultFormat){
                case "HexString":
                    result = HexDump.toHexString(data);
                    break;
                default:
                    result = HexDump.dumpHexString(data);
                    break;
            }
        }
        if (isOpenUDP){
            if(explain){
                //含有业务解析
                Long l = parseLong(result.substring(6,14), 16);
                Float f = Float.intBitsToFloat(l.intValue());
                sendUTP(code+":"+String.valueOf(f));
            }else {
                //默认不含业务解析
                sendUTP(result);
            }
        }
        Map<String,Object> params=new HashMap<>();
        SimpleDateFormat dateFormat =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String formattedDate = dateFormat.format(new Date());
        if(explain){
            //含有业务解析
            Long l = parseLong(result.substring(6,14), 16);
            Float f = Float.intBitsToFloat(l.intValue());
            params.put("data",String.valueOf(f));
        }else {
            //默认不含业务解析
            params.put("data",result);
        }
        params.put("time",formattedDate);
        mUniSDKInstance.fireGlobalEventCallback("usbListener", params);
    }

    @Override
    public void onRunError(Exception e) {
        disconnect();
    }

    /**
     *循环发送数据线程
     */
    class Sned implements Runnable{
        private UsbSerialPort usbSerialPort;
        private byte[] databyte;
        private int intervalTime;

        public Sned(UsbSerialPort usbSerialPort , byte[] databyte ,int intervalTime) {
            this.usbSerialPort = usbSerialPort;
            this.databyte = databyte;
            this.intervalTime = intervalTime;
        }
        @Override
        public void run() {
            try {
                while (true){
                    usbSerialPort.write(databyte, WRITE_WAIT_MILLIS);
                    if (Thread.currentThread().isInterrupted()) {
                        return;
                    }
                    Thread.sleep(intervalTime);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }


    /**
     * UDP发送数据
     * @param result
     */
    public void sendUTP(String result){
        try {
            DatagramPacket packet = new DatagramPacket(new byte[result.getBytes().length], result.getBytes().length, address, port);
            packet.setData(result.getBytes());
            datagramSocket.send(packet);
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * 字节数组转float
     * 采⽤IEEE 754标准
     * @param
     * @return
     */
    public static long parseLong(String s, int radix) throws NumberFormatException {
        long result = 0;
        boolean negative = false;
        int i = 0, len = s.length();
        long limit = -Long.MAX_VALUE;
        long multmin;
        int digit;
        if (len > 0) {
            char firstChar = s.charAt(0);
            if (firstChar < '0') { // Possible leading "+" or "-"
                if (firstChar == '-') {
                    negative = true;
                    limit = Long.MIN_VALUE;
                } else if (firstChar != '+')
                    if (len == 1) // Cannot have lone "+" or "-"
                        i++;
            }
            multmin = limit / radix;
            while (i < len) {
                digit = Character.digit(s.charAt(i++), radix);
                result *= radix;
                result -= digit;
            }
        }
        return negative ? result : -result;
    }
}
